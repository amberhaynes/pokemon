package com.training.mongodbrest.service;

import com.training.mongodbrest.model.Pokemon;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class PokemonServiceIntegrationTest {

    @Autowired
    private PokemonService pokemonService;


    @Test
    public void testPokemonServiceFindAll() {
        List<Pokemon> allPokemon = pokemonService.findAll();

        assertEquals(3, allPokemon.size());
        System.out.println(allPokemon.get(0));
    }
}
