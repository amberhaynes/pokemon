package com.training.mongodbrest.service;

import com.training.mongodbrest.model.Pokemon;
import com.training.mongodbrest.repository.PokemonRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest
public class PokemonServiceTest {

    @Autowired
    private PokemonService pokemonService;

    @MockBean
    private PokemonRepository pokemonRepository;

    @Test
    public void testPokemonServiceFindAll() {
        List<Pokemon> allPokemon = new ArrayList<Pokemon>();
        Pokemon testPokemon = new Pokemon();
        testPokemon.setId("adbcd");
        allPokemon.add(testPokemon);

        when(pokemonRepository.findAll()).thenReturn(allPokemon);

        assertEquals(1, pokemonService.findAll().size());
    }
}
