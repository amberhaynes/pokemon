package com.training.mongodbrest.controller;

import com.training.mongodbrest.model.Pokemon;
import com.training.mongodbrest.service.PokemonServiceInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/pokemon")
public class PokemonController {

    private static final Logger LOG = LoggerFactory.getLogger(PokemonController.class);

    @Autowired
    private PokemonServiceInterface pokemonService;

    @GetMapping
    public List<Pokemon> findAll() {
        return pokemonService.findAll();
    }

    @GetMapping("{id}")
    public ResponseEntity<Pokemon> findById(@PathVariable String id) {
        LOG.debug("findbyID being called");
        try {
            return new ResponseEntity<Pokemon>(pokemonService.findById(id).get(), HttpStatus.OK);
        } catch (NoSuchElementException ex) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public Pokemon save(@RequestBody Pokemon pokemon) {
        return pokemonService.save(pokemon);
    }

    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable String id) {
        if(pokemonService.findById(id).isEmpty()) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        pokemonService.delete(id);

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
